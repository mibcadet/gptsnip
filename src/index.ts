import OpenAI from "openai";
import process from "process";
import express from "express";
import bodyParser from "body-parser";

const openAi = new OpenAI({
  apiKey: process.env.GPT_API,
});

async function gptAsk({ question }: { question: string }) {
  let resp = "";
  const stream = await openAi.chat.completions.create({
    model: "gpt-4",
    messages: [{ role: "user", content: question }],
    stream: true,
  });
  for await (const chunk of stream) {
    const content = chunk.choices[0]?.delta?.content;
    if (content) {
      resp += content;
    }
  }
  return resp;
}

const app = express();
app.use(bodyParser.json());
const port = process.env.PORT ?? 58585;

app.post("/v1/gpt/ask", async (req, res) => {
  try {
    if (process.env.GPT_API === req.headers.authorization) {
      const result = await gptAsk({
        question: req.body.question,
      });

      res.setHeader("Content-Type", "application/json");
      res.end(JSON.stringify({ result }, null, 2));
    } else {
      res.end('{"error": "Auth failure"}');
    }
  } catch (err) {}
});

app.listen(port, () => {
  console.log(`Listening on ${port}`);
});
